﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace DependencyInjection
{
   
    class Program
    {
        static void Main(string[] args)
        {
            // Constructor : Create Parameterized Object of EmployeeBL
            EmployeeBL employeeBL = new EmployeeBL(new EmployeeDAL());

            /* Property and Method DI*/
            //EmployeeBL employeeBL = new EmployeeBL();
            // Property : Create object of EmployeeBL class 
            //employeeBL.employeeDataObject = new EmployeeDAL();

            // Method : Call to GetAllEmployees method with proper object.  
            //List<Employee> ListEmployee = employeeBL.GetAllEmployees(new EmployeeDAL());

            List<Employee> ListEmployee = employeeBL.GetAllEmployees();
            foreach(Employee emp in ListEmployee)
            {
                Console.WriteLine("ID = {0}, Name = {1}, Department = {2}", emp.ID, emp.Name, emp.Department);
                Console.ReadKey();
            }
        }
    }
}
