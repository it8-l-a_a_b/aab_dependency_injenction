﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyInjection
{
    public class EmployeeBL
    {
        public IEmployeeDAL employeeDAL;

        // Constructor Injection
        public EmployeeBL(IEmployeeDAL employeeDAL)
        {
            this.employeeDAL = employeeDAL;
        }


        // Property Injection
        //public IEmployeeDAL employeeDataObject
        //{
        //    set
        //    {
        //        this.employeeDAL = value;
        //    }
        //    get
        //    {
        //        if (employeeDataObject == null)
        //        {
        //            throw new Exception("Employee is not initialized");
        //        }
        //        else
        //        {
        //            return employeeDAL;
        //        }
        //    }
        //}

        // Method Dependency Injenction
        //public List<Employee> GetAllEmployees(IEmployeeDAL _employeeDAL)
        //{
        //    employeeDAL = _employeeDAL;
        //    return employeeDAL.SelectAllEmployees();
        //}

        public List<Employee> GetAllEmployees()
        {
            return employeeDAL.SelectAllEmployees();
        }
    }

}
